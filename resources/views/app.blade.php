<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title inertia>{{ config('app.name', 'Laravel') }}</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ mix('css/app.css') }}">

        <!-- Scripts -->
        @routes
        <script src="{{ mix('js/app.js') }}" defer></script>
        

        <!-- CSS Files
        ================================================== -->
        <link href="/showcase/css/bootstrap.min-1.css" rel="stylesheet" type="text/css">
        <link href="/showcase/css/jpreloader-1.css" rel="stylesheet" type="text/css">
        <link href="/showcase/css/animate-1.css" rel="stylesheet" type="text/css">
        <link href="/showcase/css/owl.carousel-1.css" rel="stylesheet" type="text/css">
        <link href="/showcase/css/owl.theme-1.css" rel="stylesheet" type="text/css">
        <link href="/showcase/css/owl.transitions-1.css" rel="stylesheet" type="text/css">
        <link href="/showcase/css/magnific-popup-1.css" rel="stylesheet" type="text/css">
        <link href="/showcase/css/jquery.countdown-1.css" rel="stylesheet" type="text/css">
        <link href="/showcase/css/style-1.css" rel="stylesheet" type="text/css">
        <!-- color scheme -->
        <link id="colors" href="/showcase/css/colors/scheme-02-1.css" rel="stylesheet" type="text/css">
        <link href="/showcase/css/coloring-1.css" rel="stylesheet" type="text/css">

        <!-- All Scripts Portofilio -->
        <!-- Javascript Files
        ================================================== -->
        <script src="/showcase/js/jquery.min-1.js" defer></script>
        <script src="/showcase/js/jpreLoader.min-1.js" defer></script>
        <script src="/showcase/js/bootstrap.min-1.js" defer></script>
        <script src="/showcase/js/wow.min-1.js" defer></script>
        <script src="/showcase/js/jquery.isotope.min-1.js" defer></script>
        <script src="/showcase/js/easing-1.js" defer></script>
        <script src="/showcase/js/owl.carousel-1.js" defer></script>
        <script src="/showcase/js/validation-1.js" defer></script>
        <script src="/showcase/js/jquery.magnific-popup.min-1.js" defer></script>
        <script src="/showcase/js/enquire.min-1.js" defer></script>
        <script src="/showcase/js/jquery.stellar.min-1.js" defer></script>
        <script src="/showcase/js/jquery.plugin-1.js" defer></script>
        <script src="/showcase/js/typed-1.js" defer></script>
        <script src="/showcase/js/typed-custom-1.js" defer></script>
        <script src="/showcase/js/particles-1.js" defer></script>
        <script src="/showcase/js/app-1.js" defer></script>
        <script src="/showcase/js/designesia-1.js" defer></script>

    </head>
    <body class="font-sans antialiased">
        @inertia

        @env ('local')
            <script src="http://localhost:3000/browser-sync/browser-sync-client.js"></script>
        @endenv
    </body>
</html>
